# -------------------------------------------------------------
# csv2turtle.py
# Yetanother Ontological Dialog Architecture - YODA
# Copyright (c) David Cohen, 2015
# david.cohen@sv.cmu.edu
# Derived from csv2turtle.py, Copyright (c) Anushka Bajpai, David Cohen, 2015
# This file is part of Yetanother Ontological Dialog Architecture (YODA).

# YODA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# YODA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with YODA.  If not, see <http://www.gnu.org/licenses/>.
# -------------------------------------------------------------

import re

schedule_csv_filename = "cmu_schedule.csv"
output_filename = "cmu_schedule.turtle"

class Department:
    counter=0
    def __init__(self, name):
        self.name = name
        self.URI = "base:dept_"+str(Department.counter).zfill(5)
        Department.counter += 1
    
    def write_turtle(self, outfile):
        outfile.write(self.URI + " rdf:type base:Department .\n")
        outfile.write(self.URI + " " + 'rdfs:label "' + self.name + '"^^xsd:string .\n')

class Room:
    counter=0
    def __init__(self, name):
        self.name = name
        self.URI = "base:room_"+str(Room.counter).zfill(5)
        Room.counter += 1
    
    def write_turtle(self, outfile):
        outfile.write(self.URI + " rdf:type base:Room .\n")
        outfile.write(self.URI + ' rdfs:label "' + self.name + '"^^xsd:string .\n')
        
        
class Course:
    counter=0
    def __init__(self, name, course_id, units):
        self.name = name;
        self.course_id = course_id;
        self.units = units;
        self.URI = "base:course_"+str(Course.counter).zfill(5)
        Course.counter += 1
        
    def write_turtle(self, outfile):
        outfile.write(self.URI + ' rdf:type base:Course .\n')
        outfile.write(self.URI + ' rdfs:label "' + self.name + '"^^xsd:string .\n')
        outfile.write(self.URI + ' base:has_courseid ' + self.course_id + ' .\n')
        outfile.write(self.URI + ' base:has_units ' + str(self.units) + ' .\n')

class Section:
    counter=0
    def __init__(self, name, course, room, instructors, location, days, begin, end):
        self.name = name
        self.course = course
        self.room = room
        self.instructors = instructors
        self.location = location
        self.days = days
        self.begin = begin
        self.end = end
        self.URI = "base:sec_"+str(Section.counter).zfill(5)
        Section.counter += 1
        
    def write_turtle(self, outfile):
        outfile.write(self.URI + ' rdf:type base:Section .\n')
        outfile.write(self.URI + ' rdfs:label "' + self.name + '"^^xsd:string .\n')
        outfile.write(self.URI + ' base:has_course ' + str(self.course.URI) + ' .\n')
        if (self.days != ""):
            outfile.write(self.URI + ' base:has_day "' + self.days + '"^^xsd:string .\n')
        if (self.begin != ""):
            outfile.write(self.URI + ' base:has_begin_time "' + self.begin + '"^^xsd:string .\n')
        if (self.end != ""):
            outfile.write(self.URI + ' base:has_end_time "' + self.end + '"^^xsd:string .\n')
        if (self.room != ""):
            outfile.write(self.URI + ' base:has_room ' + self.room.URI + ' .\n' )
        for instructor in self.instructors:
            if (instructor != ""):
                outfile.write(self.URI + ' base:has_instructor ' + instructor.URI + ' .\n')
        if (self.location != ""):
            outfile.write(self.URI + ' base:has_location ' + self.location.URI + ' .\n')

        
class Instructor:
    counter=0
    def __init__(self, instructor):
        self.instructor = instructor
        self.URI = "base:ins_"+str(Instructor.counter).zfill(5)
        Instructor.counter += 1
            
    def write_turtle(self, outfile):
        outfile.write(self.URI + ' rdf:type base:Instructor .\n')
        outfile.write(self.URI + ' rdfs:label "' + self.instructor + '"^^xsd:string .\n')

class Campus:
    counter=0
    def __init__(self, name, location_name):
        self.name = name
        self.location_name = location_name
        self.URI = "base:campus_"+str(Campus.counter).zfill(5)
        Campus.counter += 1
        
    def write_turtle(self, outfile):
        outfile.write(self.URI + ' rdf:type base:Campus .\n')
        outfile.write(self.URI + ' rdfs:label "' + self.name + '"^^xsd:string .\n')
        
        
def addHeader(outfile):
    outfile.write("""@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix base: <http://sv.cmu.edu/yoda#> .
""")


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
    

if __name__=="__main__":
    sections = set()
    departments_by_name = dict()
    courses_by_id = dict()
    campuses_by_location_name = dict()
    instructors_by_name = dict()
    rooms_by_name = dict()

    # manually add campuses
    campus = Campus("Pittsburgh",  'Pittsburgh/ Pennsylvania')
    campuses_by_location_name[campus.location_name] = campus
    campuses_by_location_name['Pre-College Pittsburgh'] = campus
    campus = Campus("Qatar",  'Doha/ Qatar')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("Los Angeles",  'Los Angeles/ California')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("New York",  'New York/ New York')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("Washington DC", 'Washington/ District of Columbia:Dulles')
    campuses_by_location_name[campus.location_name] = campus
    campuses_by_location_name['Washington/ District of Columbia'] = campus
    campus = Campus("Rwanda", 'Kigali/ Rwanda')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("Silicon Valley", 'San Jose/ California')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("Portugal", 'Lisbon/ Portugal')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("Australia", 'Adelaide/ Australia')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("Nanking", 'Nanking/ China')
    campuses_by_location_name[campus.location_name] = campus
    campus = Campus("Manchester", 'Manchester/ United Kingdom')
    campuses_by_location_name[campus.location_name] = campus

    firstRow = True
    course_id = None
    title = None
    units = None
    room_name = None
    location_name = None

    # load database into object oriented format
    for line in open(schedule_csv_filename, 'r'):
        matches = re.findall(r'\"(.+?)\"', line)
        for match in matches:
            line = line.replace(match,match.replace(',','/'))
        line = line.replace('"', '').replace("\xc2\xa0", "")

        if firstRow:
            firstRow = False
            continue
        
        [tempid, tempTitle, tempUnits, section, days, begin, end, tmp_room_name, tmp_location_name, instructor_names, ignore] = line.split(",")
        if (not tempid.isdigit() and tempid != '' and tempid != '\xc2\xa0'):
            department_name = tempid
            continue

        if(len(tempid) > 3):
            course_id = tempid

        if is_number(tempUnits.strip()):
            units = float(tempUnits.strip())
        elif len(tempUnits.strip())==0:
            pass                # (take previous units)
        else:
            continue            # ignore courses / sections with variable units

        if (len(tempTitle) > 3):
            course_title = tempTitle

        if (len(tmp_room_name.strip()) > 2):
            room_name = tmp_room_name.strip()

        if tmp_location_name in campuses_by_location_name:
            location_name = tmp_location_name

        instructors = []
        for instructor_name in instructor_names.split("/"):
            instructor_name = instructor_name.strip()
            if instructor_name in instructors_by_name:
                instructors.append(instructors_by_name[instructor_name])
            else:
                instructor = Instructor(instructor_name)
                instructors_by_name[instructor_name] = instructor
                instructors.append(instructors_by_name[instructor_name])

        if department_name in departments_by_name:
            department = departments_by_name[department_name]
        else:
            department = Department(department_name)
            departments_by_name[department_name] = department

        if course_id in courses_by_id:
            course = courses_by_id[course_id]
        else:
            course = Course(course_title, course_id, units)
            courses_by_id[course_id] = course

        if room_name in rooms_by_name:
            room = rooms_by_name[room_name]
        elif room_name is None:
            pass
        else:
            room = Room(room_name)
            rooms_by_name[room_name] = room

        location = campuses_by_location_name[location_name]

        sect = Section(section, course, room, instructors, location, days, begin, end)
        sections.add(sect)

    print "sections", len(sections)
    print "courses", len(courses_by_id)
    print "instructors", len(instructors_by_name)
    print "departments", len(departments_by_name)
    print "rooms", len(rooms_by_name)


    # write out turtle file
    outfile = open(output_filename, 'w+')
    addHeader(outfile)
    for c in sorted(set(campuses_by_location_name.values()), key=lambda x: x.URI):
        c.write_turtle(outfile)
    for d in sorted(departments_by_name.values(), key=lambda x: x.URI):
        d.write_turtle(outfile)
    for i in sorted(instructors_by_name.values(), key=lambda x: x.URI):
        i.write_turtle(outfile)
    for r in sorted(rooms_by_name.values(), key=lambda x: x.URI):
        r.write_turtle(outfile)
    for c in sorted(courses_by_id.values(), key=lambda x: x.URI):
        c.write_turtle(outfile)
    for s in sorted(sections, key=lambda x: x.URI):
        s.write_turtle(outfile)
    outfile.close()
